// Copyright 2021 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package bot

import (
	"context"
	"math/rand"
	"strconv"
	"testing"
	"time"

	mevent "maunium.net/go/mautrix/event"
	mid "maunium.net/go/mautrix/id"
)

var configTests = []struct{ in, out *ClientConfig }{
	{
		in: nil,
		out: &ClientConfig{
			MessageType: "m.notice",
			Commands:    map[string]*Command{"help": new(Command)},
		},
	},
	{
		in: &ClientConfig{
			MessageType: "test",
			Commands:    map[string]*Command{},
		},
		out: &ClientConfig{
			MessageType: "test",
			Commands:    map[string]*Command{},
		},
	},
}

func TestNewClientErr(t *testing.T) {
	_, err := NewClient("\000", "", "", nil)
	if err == nil {
		t.Errorf("Expected error for invalid URL, got nil")
	}
}

func TestNewClient(t *testing.T) {
	for _, test := range configTests {
		url := "https://" + strconv.Itoa(rand.Int())
		uid := "@" + strconv.Itoa(rand.Int()) + ":example.com"
		token := strconv.Itoa(rand.Int())

		c, err := NewClient(url, uid, token, test.in)
		if err != nil {
			t.Errorf("Error creating client using url: %q, uid: %q, token: %q, err: %q",
				url, uid, token, err)
			continue
		}

		if c.Client.HomeserverURL.String() != url {
			t.Errorf("Unexpected homeserver URL: expected %q, got %q", url, c.Client.HomeserverURL.String())
		}

		if c.Client.UserID.String() != uid {
			t.Errorf("Unexpected user ID: expected %q, got %q", uid, c.Client.UserID)
		}

		if c.Client.AccessToken != token {
			t.Errorf("Unexpected access token: expected %q, got %q", token, c.Client.AccessToken)
		}

		if c.Config.MessageType != test.out.MessageType {
			t.Errorf("Unexpected message type: expected %q, got %q", test.out.MessageType, c.Config.MessageType)
		}

		if len(c.Config.Commands) != len(test.out.Commands) {
			t.Errorf("Unexpected commands: expected %#v, got %#v", test.out.Commands, c.Config.Commands)
			continue
		}

		for k := range test.out.Commands {
			if _, ok := c.Config.Commands[k]; !ok {
				t.Errorf("Unexpected commands: expected %#v, got %#v", test.out.Commands, c.Config.Commands)
				break
			}
		}
	}
}

func TestClient_Run(t *testing.T) {
	c, err := NewClient("", "@test:example.com", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	// Create a test client which only returns 404s
	c.Client.Client = NewTestClient(map[string]string{})

	err = c.Run(context.Background())
	if err == nil {
		t.Errorf("Expected error for Run, got nil")
	}

	exp := "sync error: failed to POST /_matrix/client/v3/user/@test:example.com/filter: HTTP 404"
	if err.Error() != exp {
		t.Errorf("Incorrect error: expected %q, got %q", exp, err.Error())
	}
}

func TestClient_Stop(t *testing.T) {
	c, err := NewClient("", "@test:example.com", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	c.Client.Client = NewTestClient(map[string]string{
		`/_matrix/client/v3/sync\?timeout=\d+`:             `{}`,
		"/_matrix/client/v3/user/@test:example.com/filter": `{}`,
	})

	done := make(chan bool)
	test := func() {
		err = c.Run(context.Background())
		if err != nil {
			t.Errorf("Unexpected error for Run: %q", err)
		}
		done <- true
	}

	// Run the syncer
	go test()

	// Wait a bit, and stop the syncer
	time.Sleep(10 * time.Millisecond)
	c.Stop()

	// Wait for the syncer to exit
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	select {
	case <-ctx.Done():
		t.Fatal("Deadline exceeded")
	case <-done:
		return
	}
}

func TestClient_NewRoom(t *testing.T) {
	i := mid.RoomID(strconv.Itoa(rand.Int()))

	c, err := NewClient("", "@test:example.com", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	r := c.NewRoom(i)
	if r.ID != i {
		t.Errorf("Incorrect room ID: expected %q, got %q", i, r.ID)
	}

	if r.ID != i {
		t.Errorf("Incorrect room client: expected %#v, got %#v", c, r.client)
	}
}

func TestClient_SetCommand(t *testing.T) {
	c, err := NewClient("", "@test:example.com", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	cmd := new(Command)
	c.SetCommand("test", cmd)

	if c.Config.Commands["test"] != cmd {
		t.Errorf("Incorrect command: expected %#v, got %#v", cmd, c.Config.Commands["test"])
	}
}

var messageTests = []struct {
	in  *Event
	out *Message
}{
	{
		in: &Event{&mevent.Event{
			Content: mevent.Content{
				VeryRaw: []byte("{\"body\": \"help I'm being ignored\"}"),
			},
		}},
		out: nil,
	},
	{
		in: &Event{&mevent.Event{
			Sender: "@test:example.com",
			Content: mevent.Content{
				VeryRaw: []byte("{\"body\": \"I sent this myself\"}"),
			},
		}},
		out: nil,
	},
	{
		in: &Event{&mevent.Event{
			Content: mevent.Content{
				VeryRaw: []byte("{\"body\": \"!help\"}"),
			},
		}},
		out: &Message{
			Body: "- `help`: Shows help for a command.",
		},
	},
	{
		in: &Event{&mevent.Event{
			Content: mevent.Content{
				VeryRaw: []byte("{\"body\": \"Test: help\"}"),
			},
		}},
		out: &Message{
			Body: "- `help`: Shows help for a command.",
		},
	},
	{
		in: &Event{&mevent.Event{
			Content: mevent.Content{
				VeryRaw: []byte("{\"body\": \"@test:example.com: help\"}"),
			},
		}},
		out: &Message{
			Body: "- `help`: Shows help for a command.",
		},
	},
	{
		in: &Event{&mevent.Event{
			Content: mevent.Content{
				VeryRaw: []byte("{\"body\": \"!invalid\"}"),
			},
		}},
		out: &Message{
			Body: "unknown command: `invalid`",
		},
	},
	{
		in: &Event{&mevent.Event{
			Content: mevent.Content{
				VeryRaw: []byte(`{"body": "!invalid arg1\narg2"}`),
			},
		}},
		out: &Message{
			Body: "unknown command: `invalid` (args: `arg1`, `\\narg2`)",
		},
	},
}

func TestClient_handleCommand(t *testing.T) {
	c, err := NewClient("", "@test:example.com", "",
		&ClientConfig{CommandPrefixes: []string{"!"}})
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	c.Client.Client = NewTestClient(map[string]string{
		"/_matrix/client/v3/profile/@test:example.com/displayname": `{"displayname": "Test"}`,
	})

	for i, test := range messageTests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			m := c.handleCommand(context.Background(), test.in)
			if m == nil {
				if test.out == nil {
					return
				}

				t.Errorf("Incorrect body for message %q: expected %#v, got %#v",
					test.in.Content.AsMessage().Body, test.out, m)

				return
			}

			if m.Body != test.out.Body {
				t.Errorf("Incorrect body for message %q: expected %q, got %q",
					test.in.Content.AsMessage().Body, test.out.Body, m.Body)
			}
		})
	}
}
