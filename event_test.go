package bot_test

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/slxh/matrix/bot"
	mevent "maunium.net/go/mautrix/event"
)

func TestEvent_Time(t *testing.T) {
	event := bot.Event{Event: &mevent.Event{Timestamp: 1722175442664}}
	exp := time.Unix(1722175442, 664000000)

	if event.Time() != exp {
		t.Error("time mismatch")
	}
}

func TestEvent_MessageEventContent(t *testing.T) {
	tests := map[string]struct {
		Event bot.Event
		Exp   *mevent.MessageEventContent
		Err   require.ErrorAssertionFunc
	}{
		"parsed & ok": {
			Event: bot.Event{Event: &mevent.Event{
				Content: mevent.Content{
					Parsed: &mevent.MessageEventContent{Body: "yes"},
				},
			}},
			Exp: &mevent.MessageEventContent{Body: "yes"},
			Err: require.NoError,
		},
		"parsed & error": {
			Event: bot.Event{Event: &mevent.Event{
				Content: mevent.Content{
					Parsed: &mevent.MemberEventContent{},
				},
			}},
			Exp: nil,
			Err: func(t require.TestingT, err error, i ...interface{}) {
				require.ErrorIs(t, err, bot.ErrNotAMessage, i...)
			},
		},
		"not parsed & ok": {
			Event: bot.Event{Event: &mevent.Event{
				Content: mevent.Content{
					VeryRaw: json.RawMessage(`{"body":"yes"}`),
				},
			}},
			Exp: &mevent.MessageEventContent{Body: "yes"},
			Err: require.NoError,
		},
		"not parsed & error ": {
			Event: bot.Event{Event: &mevent.Event{
				Content: mevent.Content{
					VeryRaw: json.RawMessage(`{`),
				},
			}},
			Err: func(t require.TestingT, err error, i ...interface{}) {
				require.ErrorContains(t, err, "unexpected end of JSON input", i...)
			},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			c, err := tc.Event.MessageEventContent()
			tc.Err(t, err)

			assert.Equal(t, tc.Exp, c)
		})
	}
}
