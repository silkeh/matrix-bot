// Copyright 2021 Silke Hofstra
//
// Licensed under the EUPL (the "Licence");
//
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and
// limitations under the Licence.

package bot

import (
	"io"
	"net/http"
	"regexp"
	"strings"
)

type TestRoundTripper struct {
	Responses map[string]string
	Requests  []*http.Request
}

func (t *TestRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	t.Requests = append(t.Requests, req)

	url := req.URL.String()
	for regex, resp := range t.Responses {
		if regexp.MustCompile(regex).MatchString(url) {
			return &http.Response{
				StatusCode: http.StatusOK,
				Body:       io.NopCloser(strings.NewReader(resp)),
			}, nil
		}
	}

	return &http.Response{StatusCode: http.StatusNotFound}, nil
}

func NewTestClient(tripper map[string]string) *http.Client {
	return &http.Client{
		Transport: &TestRoundTripper{
			Responses: tripper,
		},
	}
}
