package bot

import (
	"context"
	"fmt"
	"io"
	"strings"
	"testing"

	mid "maunium.net/go/mautrix/id"
)

var messageEventID = mid.EventID("$YUwRidLecu:example.com")

func roomTest(t *testing.T, send func(*Room) (mid.EventID, error), expBody string) {
	t.Helper()

	c, err := NewClient("", "@test:example.com", "",
		&ClientConfig{CommandPrefixes: []string{"!"}})
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	roundTripper := &TestRoundTripper{Responses: map[string]string{
		"/_matrix/client/v3/rooms/%21testID/send/m.room.message/.*": fmt.Sprintf(`{"event_id":%q}`, messageEventID),
		"/_matrix/client/v3/rooms/%21testID/join":                   `{"room_id": "!testID"}`,
	}}

	c.Client.Client.Transport = roundTripper

	id, err := send(c.NewRoom("!testID"))
	if err != nil {
		t.Errorf("Error sending message: %s", err)
	}

	if id != messageEventID {
		t.Errorf("Unexpected event ID: expected %q, got %q", messageEventID, id.String())
	}

	var data []byte
	if roundTripper.Requests[0].Body != nil {
		data, _ = io.ReadAll(roundTripper.Requests[0].Body)
	}

	got := strings.TrimSpace(string(data))
	if got != expBody {
		t.Errorf("Unexpected body: expected \n%q, got \n%q", expBody, got)
	}
}

func TestRoom_SendMarkdown(t *testing.T) {
	roomTest(t, func(r *Room) (mid.EventID, error) { return r.SendMarkdown(context.Background(), "*test*") },
		`{"msgtype":"m.notice","body":"*test*",`+
			`"formatted_body":"\u003cp\u003e\u003cem\u003etest\u003c/em\u003e\u003c/p\u003e\n",`+
			`"format":"org.matrix.custom.html"}`)
}

func TestRoom_SendText(t *testing.T) {
	roomTest(t, func(r *Room) (mid.EventID, error) { return r.SendText(context.Background(), "*test*") },
		`{"msgtype":"m.notice","body":"*test*"}`)
}

func TestRoom_SendHTML(t *testing.T) {
	roomTest(t, func(r *Room) (mid.EventID, error) {
		return r.SendHTML(context.Background(), "*test*", "<b><i>test</b></i>")
	},
		`{"msgtype":"m.notice","body":"*test*",`+
			`"formatted_body":"\u003cb\u003e\u003ci\u003etest\u003c/b\u003e\u003c/i\u003e",`+
			`"format":"org.matrix.custom.html"}`)
}

func TestRoom_Allowed1(t *testing.T) {
	c, err := NewClient("", "@test:example.com", "", nil)
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	if !c.NewRoom("!test").Allowed() {
		t.Errorf("Expected all rooms to be allowed, but was not.")
	}
}

func TestRoom_Allowed2(t *testing.T) {
	tests := map[string]bool{
		"!allowed": true,
		"!denied":  false,
	}

	c, err := NewClient("", "@test:example.com", "", &ClientConfig{
		AllowedRooms: []mid.RoomID{"!allowed"},
	})
	if err != nil {
		t.Fatalf("Error creating test client: %s", err)
	}

	for roomID, allowed := range tests {
		res := c.NewRoom(mid.RoomID(roomID)).Allowed()
		if res != allowed {
			t.Errorf("Incorrect allowed: expected %v, got %v", allowed, res)
		}
	}
}

func TestRoom_Join(t *testing.T) {
	testFunc := func(r *Room) (mid.EventID, error) {
		_, err := r.Join(context.Background())

		return messageEventID, err
	}

	roomTest(t, testFunc, `{}`)
}
